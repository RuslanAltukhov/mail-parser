<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 20.12.2017
 * Time: 9:29
 */

class DB
{
    private $DB;
    private static $_instance;
    private $buffer_size = 1000; //Кол-во записей, которое выводится за раз

    /**
     * Получение размера буфера
     * @return int
     */
    public function getBufferSize()
    {
        return $this->buffer_size;
    }

    private function __construct()
    {
        $this->DB = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die("Не удается установить соединение с БД");
    }

    /**
     * Получение порции записей из табоицы
     * @param $table
     * @param array $fields
     * @param int $offset
     * @param int $buffer_size
     * @return array
     */
    public function getRows($table, $fields = ['*'], $offset = 0, $buffer_size = 1000)
    {
        $result = ['offset' => $offset, 'has_next_page' => false, 'data' => []];
        //Всегда запрашиваем на одну запись больше для того чтобы понять присуncndetn ли следующая страница
        $data = $this->DB->query("SELECT " . implode(",", $fields) . " FROM {$table} LIMIT {$offset}," . ($buffer_size + 1));
        if (!is_bool($data))
            while ($row = $data->fetch_assoc())
                $result['data'][] = $row;

        if (count($result['data']) > $this->buffer_size) $result['has_next_page'] = true;
        return $result;
    }

    /**
     * Получение экземпляра БД
     * @return DB
     */
    public static function getInstance()
    {
        if (self::$_instance)
            return self::$_instance;
        else
            return self::$_instance = new self();
    }
}